# Coding Assessment

This assignment is designed to test your ability as a Full-Stack developer. You are asked
to use a public facing API to create a stylised page, as well as create an internal API to
read/write user preferences.

## Requirements

This project is done using Laravel 8 and JQuery. Also, install composer in order to be able to download app dependencies.


## Installation

Please follow the following steps.

Clone this repository `git clone https://gitlab.com/delebakare/coding-assignment.git`; copy the .env.example file into a .env file. In the .env file, edit the database name (use any random name) with your database username and password. Please ensure this database is already created in your SQL Database Management System. Once this is done, the migration command below will install all the required tables for you to use the application.  Run the following commands in the project path on the terminal :

```
run: composer update
run: php artisan migrate

```

### After installation

Enter `http://localhost:8000`; in your browser to view the application.

If any of the processes is not behaving like expected, try tailing their logs
to see what is wrong.

```
sudo tail storage/laravel.log
```


## Acknowledgments

This assignment is done Dele Bakare for DayDot.
